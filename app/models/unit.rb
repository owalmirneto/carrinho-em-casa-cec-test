class Unit < ActiveRecord::Base
  # validations
  validates :slug, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
end
