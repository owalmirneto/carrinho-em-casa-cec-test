class Product < ActiveRecord::Base
  belongs_to :supermarket
  belongs_to :unit

  # validations
  validates :supermarket, presence: true, associated: true
  validates :unit, presence: true, associated: true
  validates :slug, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
  validates :amount, presence: true, numericality: true
  validates :quantity, numericality: { only_integer: true }
end
