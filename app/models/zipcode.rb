class Zipcode < ActiveRecord::Base
  # relationships
  has_and_belongs_to_many :supermarkets

  # validations
  validates :cep, presence: true, uniqueness: true
end
