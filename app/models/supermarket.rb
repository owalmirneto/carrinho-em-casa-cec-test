class Supermarket < ActiveRecord::Base
  extend FriendlyId

  friendly_id :slug, use: [:slugged, :finders]

  # relationships
  has_and_belongs_to_many :zipcodes
  has_many :products

  # validations
  validates :slug, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
end
