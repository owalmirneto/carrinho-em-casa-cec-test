ready = ->
  # Vendors
  Bootstrap.init()
  MeioMask.init()
  JqueryValidation.init()

  # Modules
  Home.init()
  Cart.init()

$(document).ready ready
$(document).on 'page:load', ready
