//= require jquery
//= require jquery_ujs
//= require bootstrap/dist/js/bootstrap.min
//= require jquery-meiomask/dist/meiomask
//= require jquery-validation/dist/jquery.validate
//= require jquery-validation/src/localization/messages_pt_BR

//= require_tree ./initializers
//= require_tree ./modules
//= require initializers

//= require turbolinks
