@MeioMask =
  init: ->
    @simple()

  simple: ->
    $('input[data-mask]').each ->
      input = $(@)
      input.setMask input.data('mask')
