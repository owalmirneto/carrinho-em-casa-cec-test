@Home =
  init: ->
    @searchValidations()

  searchValidations: ->
    zipcode = $('#form-search-by-zipcode').find '#zipcode'

    $('#form-search-by-zipcode').validate
      rules:
        zipcode:
          cep: true
          required: true
          remote:
            type: "get"
            url: "/validations/zipcodes"
            data:
              zipcode: ->
                zipcode.val()
      messages:
        zipcode:
          cep: 'CEP inválido'
          required: 'Campo obrigatório'
          remote: 'Não há supermecados para esse CEP'
