@Cart =
  init: ->
    @increment()
    @decrement()
    @destroy()
    @clear()

  increment: ->
    Cart.sendAjax('.add-to-cart', 'increment', 'GET')

  decrement: ->
    Cart.sendAjax('.remove-to-cart', 'decrement', 'PUT')

  destroy: ->
    Cart.sendAjax('.destroy-to-cart', 'destroy', 'DELETE')

  clear: ->
    #Cart.sendAjax()

  sendAjax: (selector, action, type) ->
    $('#products').on 'click', selector, ->
      slug = $(@).closest('div').data('product-code')
      Loading.open()

      $.ajax(
        url: "/cart/#{slug}/#{action}"
        type: type
        success: (response) ->
          $('#cart_products').html response
      ).always ->
        Loading.close()
