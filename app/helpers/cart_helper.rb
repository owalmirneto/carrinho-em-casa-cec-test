module CartHelper
  def cart_total products
    total = 0
    products.decorate.each do |product|
      total = total + product.total(session)
    end
    number_to_currency total
  end
end
