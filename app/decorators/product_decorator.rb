class ProductDecorator < Draper::Decorator
  delegate_all

  def unit_name
    object.unit.name if object.unit
  end

  def amount_to_currency
    h.number_to_currency object.amount
  end

  def quantity_unit
    "#{object.quantity} #{unit_name}"
  end

  def cart_quantity(session)
    session[:product_ids].count(object.slug)
  end

  def total(session)
    cart_quantity(session) * object.amount
  end

  def total_to_currency(session)
    h.number_to_currency total(session)
  end
end
