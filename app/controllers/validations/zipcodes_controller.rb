module Validations
  class ZipcodesController < ApplicationController
    def index
      zipcode = Zipcode.find_by_cep params[:zipcode]

      render json: zipcode.present? && zipcode.supermarkets.any?
    end
  end
end
