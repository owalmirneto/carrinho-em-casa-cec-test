class CartController < ApplicationController
  layout false

  def increment
    cart_increment(params[:code])
    set_products

    render :index
  end

  def decrement
    cart_decrement(params[:code])
    set_products

    render :index
  end

  def destroy
    session[:product_ids].delete(params[:code])
    set_products

    render :index
  end

  def clear
    session[:product_ids] = []
    set_products

    render :index
  end

  private

  def set_products
    @products = Product.where(slug: session[:product_ids]) if session[:product_ids]
  end

  def cart_decrement(code)
    products = session[:product_ids]
    equals = []

    products.each { |product| equals << product if product == code }
    products.delete(code)

    equals.pop
    session[:product_ids] = products + equals
  end

  def cart_increment(code)
    session[:product_ids] ||= []
    session[:product_ids] << code if code
  end
end
