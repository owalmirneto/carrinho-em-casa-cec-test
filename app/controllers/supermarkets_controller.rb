class SupermarketsController < ApplicationController
  def index
    @zipcode = Zipcode.find_by_cep(params[:zipcode])
    @supermarkets = @zipcode.supermarkets if @zipcode
  end

  def show
    @cart_products = Product.where(slug: session[:product_ids]) if session[:product_ids]

    @supermarket = Supermarket.find(params[:id])
    @products = @supermarket.products if @supermarket
  end
end
