require 'rails_helper'

RSpec.describe Zipcode, type: :model do
  context 'model validations' do
    it { should have_and_belong_to_many(:supermarkets) }
    it { is_expected.to validate_presence_of(:cep) }
    it { is_expected.to validate_uniqueness_of(:cep) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:cep).of_type(:string) }
  end
end
