require 'rails_helper'

RSpec.describe Product, type: :model do
  context 'model relationships' do
    it { should belong_to(:supermarket) }
    it { should belong_to(:unit) }
  end
  context 'model validations' do
    it { is_expected.to validate_presence_of(:slug) }
    it { is_expected.to validate_uniqueness_of(:slug) }
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name) }
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_numericality_of(:amount) }
    it { is_expected.to validate_numericality_of(:quantity) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:supermarket_id).of_type(:integer) }
    it { is_expected.to have_db_column(:unit_id).of_type(:integer) }
    it { is_expected.to have_db_column(:slug).of_type(:string) }
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:amount).of_type(:decimal) }
    it { is_expected.to have_db_column(:quantity).of_type(:integer) }
    it { is_expected.to have_db_column(:image_url).of_type(:string) }
  end
end
