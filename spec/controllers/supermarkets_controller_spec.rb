require 'rails_helper'

RSpec.describe SupermarketsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:supermarket) { Supermarket.create(slug: 'name', name: 'Name') }
    it "returns http success" do
      get :show, id: supermarket.slug
      expect(response).to have_http_status(:success)
    end
  end

end
