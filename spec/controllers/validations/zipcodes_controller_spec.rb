require 'rails_helper'

module Validations
  RSpec.describe ZipcodesController, type: :controller do

    describe "GET #index" do
      it "returns http success" do
        get :index
        expect(response).to have_http_status(:success)
      end
    end
  end
end
