class AddImageUrlToSupermarkets < ActiveRecord::Migration
  def change
    add_column :supermarkets, :image_url, :string
  end
end
