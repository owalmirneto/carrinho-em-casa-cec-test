class CreateSupermarketsZipcodes < ActiveRecord::Migration
  def change
    create_table :supermarkets_zipcodes, id: false do |t|
      t.references :supermarket
      t.references :zipcode
    end
    add_index :supermarkets_zipcodes, [:supermarket_id, :zipcode_id]
    add_index :supermarkets_zipcodes, :zipcode_id
  end
end
