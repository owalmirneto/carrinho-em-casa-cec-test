class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :supermarket, index: true, foreign_key: true
      t.references :unit, index: true, foreign_key: true
      t.string :slug, unique: true
      t.string :name, unique: true
      t.decimal :amount
      t.integer :quantity, default: 1

      t.timestamps null: false
    end
  end
end
