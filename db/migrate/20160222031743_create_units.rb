class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.string :slug, unique: true
      t.string :name, unique: true

      t.timestamps null: false
    end
  end
end
