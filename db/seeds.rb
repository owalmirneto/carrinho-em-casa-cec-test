ActiveRecord::Base.transaction do
  # Zipcodes
  z01202000 = Zipcode.create!(cep: '01202-000')
  z01311000 = Zipcode.create!(cep: '01311-000')
  z01217000 = Zipcode.create!(cep: '01217-000')
  z54759195 = Zipcode.create!(cep: '54759-195')
  z52061200 = Zipcode.create!(cep: '52061-200')

  # Units
  unidade = Unit.create!(slug: 'un', name: 'unidade')
  grama = Unit.create!(slug: 'g', name: 'grama')
  quilograma = Unit.create!(slug: 'kg', name: 'quilograma')
  mililitro = Unit.create!(slug: 'ml', name: 'mililitro')
  litro = Unit.create!(slug: 'l', name: 'litro')

  # Carrinho em casa
  carrinho_em_casa = Supermarket.create!(slug: 'carrinho-em-casa', name: 'Carrinho em casa',
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/supermarkets/images/000/000/167/original/Carrinho_em_Casa.png',
    zipcodes: [z01202000, z01311000, z01217000])
  Product.create!(
    supermarket: carrinho_em_casa,
    unit: unidade,
    slug: 'melancia-inteira',
    name: 'Melancia Inteira',
    amount: 16.23,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/299/large/melancia.jpg')
  Product.create!(
    supermarket: carrinho_em_casa,
    unit: unidade,
    slug: 'mamao-formosa',
    name: 'Mamão Formosa',
    amount: 10.66,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/300/large/487319.jpg')
  Product.create!(
    supermarket: carrinho_em_casa,
    unit: unidade,
    slug: 'goiaba-branca',
    name: 'Goiaba Branca',
    amount: 2.17,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/303/large/Goiba_branca__77274_zoom.jpg')

  # Casa Santa Luzia
  casa_santa_luzia = Supermarket.create!(slug: 'casa-santa-luzia', name: 'Casa Santa Luzia',
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/supermarkets/images/000/000/016/original/Casa_Santa_Luzia_OS.png',
    zipcodes: [z01202000, z01311000, z01217000])
  Product.create!(
    supermarket: casa_santa_luzia,
    unit: litro,
    slug: 'leite-semi-desnatado-batavo',
    name: 'Leite Semi Desnatado Batavo',
    amount: 3.57,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/941/large/0010653_leite-uht-batavo-semidesnatado-1l.png?1452107669')
  Product.create!(
    supermarket: casa_santa_luzia,
    unit: litro,
    slug: 'leite-integral-com-ferro-batavo',
    name: 'Leite Integral com Ferro Batavo',
    amount: 4.90,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/021/038/large/longa-vida-batavo-integral-com-ferro-caixa-1l_300x300-PU685cf_1.jpg')
  Product.create!(
    supermarket: casa_santa_luzia,
    unit: litro,
    slug: 'leite-longa-vida-desnatado-com-calcio-batavo',
    name: 'Leite Longa Vida Desnatado com Cálcio Batavo',
    amount: 4.70,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/021/039/large/longa-vida-batavo-desnatado-com-calcio-caixa-1l_300x300-PU685bd_1.jpg')

  # Mundo Verde
  mundo_verde = Supermarket.create!(slug: 'mundo-verde', name: 'Mundo Verde',
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/supermarkets/images/000/000/175/original/Mundo_Verde.png',
    zipcodes: [z01202000, z01311000, z01217000])
  Product.create!(
    supermarket: mundo_verde,
    unit: unidade,
    slug: 'manga-haden',
    name: 'Manga Haden',
    amount: 7.74,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/302/large/gr336comoplantar_01.jpg')
  Product.create!(
    supermarket: mundo_verde,
    unit: unidade,
    slug: 'maca-red-importada',
    name: 'Maçã Red Importada',
    amount: 2.17,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/301/large/493498.jpg')
  Product.create!(
    supermarket: mundo_verde,
    unit: litro,
    slug: 'leite-longa-vida-semi-desnatado-batavo-sensy',
    name: 'Leite Longa Vida Semi Desnatado Batavo Sensy',
    amount: 5.71,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/048/944/large/509391.jpg')

  # Nespresso
  nespresso = Supermarket.create!(slug: 'nespresso', name: 'Nespresso',
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/supermarkets/images/000/000/176/original/Nespresso.png',
    zipcodes: [z01217000, z52061200])
  Product.create!(
    supermarket: nespresso,
    unit: unidade,
    slug: 'caneca-termica-touch-travel',
    name: 'Caneca Térmica Touch Travel',
    amount: 75.00,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/072/401/large/Caneca_Termica_Touch_Travel.png')
  Product.create!(
    supermarket: nespresso,
    unit: unidade,
    slug: 'kazaar-intensidade-12-caixa-com-10-capsulas',
    name: 'Kazaar Intensidade 12 Caixa com 10 Cápsulas',
    amount: 22.75,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/072/336/large/Kazaar.png')
  Product.create!(
    supermarket: nespresso,
    unit: unidade,
    slug: 'pixie-clips-branca-e-coral-neon-110v',
    name: 'Pixie Clips Branca e Coral Neon 110v',
    amount: 8.09,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/072/361/large/Pixie_Clips_Branca_e_Coral_Neon.png')

  # Cobasi
  cobasi = Supermarket.create!(slug: 'cobasi', name: 'Cobasi',
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/supermarkets/images/000/000/177/original/Cobasi.png',
    zipcodes: [z01217000, z52061200])
  Product.create!(
    supermarket: cobasi,
    unit: unidade,
    slug: 'criadeira-rede-nb-3201-boyu',
    name: 'Criadeira Rede NB-3201 Boyu',
    amount: 24.75,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/074/306/large/Criadeira-Rede-NB-3201-Boyu.jpg')
  Product.create!(
    supermarket: cobasi,
    unit: unidade,
    slug: 'comedouro-anti-formiga-alvorada',
    name: 'Comedouro Anti-Formiga Alvorada',
    amount: 3.19,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/073/949/large/Comedouro-Anti-formigas-Gatos-Alvorada-Vermelho.png')
  Product.create!(
    supermarket: cobasi,
    unit: unidade,
    slug: 'formicida-mata-formiga-ultraverde',
    name: 'Formicida Mata Formiga Ultraverde',
    amount: 8.09,
    image_url: 'https://d2aodgfkodx7f7.cloudfront.net/products/images/000/074/535/large/Formicida-Mata-Formiga-20-ml-Ultraverde.jpg')
end
