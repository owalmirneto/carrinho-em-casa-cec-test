# Carrinho em Casa - Ruby on Rails Developer Test

## Installation

**Dependecies**

```
bundle install
```

**Environment**

```
cp .env.example .env # set your env vars
```

**Database**

```
cp config/database.yml.example config/database.yml # set your database
```

## Tests

```
bundle exec rake db:create db:migrate db:seed RAILS_ENV=test
bundle exec rspec
```

## Running

```
bundle exec rake db:create db:migrate db:seed
bundle exec rails s
```

## CEPs

 * 01202-000
 * 01311-000
 * 01217-000
 * 54759-195
 * 52061-200
