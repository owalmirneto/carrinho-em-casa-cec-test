Rails.application.routes.draw do
  root 'home#index'

  resources :supermarkets, only: [:index, :show]

  get 'cart/:code/increment', to: 'cart#increment'
  put 'cart/:code/decrement', to: 'cart#decrement'
  delete 'cart/:code/destroy', to: 'cart#destroy'
  delete 'cart/clear', to: 'cart#clear'

  namespace :validations, defaults: { format: :json } do
    get 'zipcodes', to: 'zipcodes#index'
  end
end
